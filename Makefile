.PHONY: run-logic-test
run-logic-test: ## run technical test
	docker build -t technical-test-script technical-test/
	docker run technical-test-script

.PHONY: run-backend-test
run-backend-test: ## run backend test
	docker build -t backend-test-script backend-test/
	docker run -p 3000:3000 backend-test-script

.PHONY: run-frontend-test
run-frontend-test: ## run frontend test
	docker build -t frontend-test-script react-native-test/
	docker run -it --rm -p 8081:8081 frontend-test-script

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help