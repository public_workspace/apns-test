def longest_common_prefix(strs):
    if not strs:
        return ""

    prefix = strs[0]
    for s in strs[1:]:
        while s[:len(prefix)] != prefix:
            prefix = prefix[:-1]
            if not prefix:
                return ""
    return prefix

list_of_str = [
    ["flower","flow","flight"],
    ["dog","racecar","car"],
    # add more example
    ["xlower","flow","flight"],
]

for index,str_item in enumerate(list_of_str) :
    print(f"nuber : {index + 1}")
    print(f"array : {str_item}")
    print(f"prefix result : {longest_common_prefix(str_item)}")
