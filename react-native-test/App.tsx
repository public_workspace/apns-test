import React, { useEffect, useState } from 'react';
import { View, Text, Button, TouchableOpacity, FlatList, StyleSheet, ScrollView } from 'react-native';
import questions from './questions.json';
import { shuffle } from 'lodash'; 
import AsyncStorage from '@react-native-async-storage/async-storage';

interface Question {
  question: string;
  answers: string[];
  correctAnswer: string;
}

const LeaderboardPage: React.FC = () => {
  const [leaderboardData, setLeaderboardData] = useState<{ date: string; score: number }[]>([]);
  // Dummy leaderboard data
  const Dummy_Leaderboard = [
    { score: 20, date: '2024-02-12' },
    { score: 18, date: '2024-02-10' },
    { score: 15, date: '2024-02-08' },
  ];

  useEffect(() => {
    const fetchLeaderboardData = async () => {
      try {
        const storedData = await AsyncStorage.getItem('leaderboardData');
        if (storedData) {
          const parsedData = JSON.parse(storedData);
          const combinedData = [...Dummy_Leaderboard, ...parsedData];
          combinedData.sort((a, b) => b.score - a.score);
          setLeaderboardData(combinedData);
        } else {
          setLeaderboardData([...Dummy_Leaderboard]);
        }
      } catch (error) {
        console.error('Error retrieving leaderboard data:', error);
        setLeaderboardData([...Dummy_Leaderboard]);
      }
    };
  
    fetchLeaderboardData();
  }, []);
  

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Leaderboard</Text>
      <FlatList
        data={leaderboardData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>Date : {item.date} Score : {item.score}</Text>
          </View>
        )}
      />
    </View>
  );
};
const ExamPage: React.FC<{ setCurrentPage: (page: 'Leaderboard' | 'Exam') => void }> = ({ setCurrentPage }) => {
  const [selectedAnswers, setSelectedAnswers] = useState<(number | null)[]>(Array.from({ length: questions.length }, () => null));
  const [score, setScore] = useState(0);
  const [submitted, setSubmitted] = useState(false);
  const [shuffledQuestions, setShuffledQuestions] = useState<Question[]>([]);

  useEffect(() => {
    setShuffledQuestions(shuffle(questions));
  }, []);

  const handleAnswer = (questionIndex: number, selected: number) => {
    const updatedSelectedAnswers = [...selectedAnswers];
    updatedSelectedAnswers[questionIndex] = selected;
    setSelectedAnswers(updatedSelectedAnswers);
  };

  const handleSubmit = async () => {
    let totalScore = 0;
    questions.forEach((question, index) => {
      if (selectedAnswers[index] !== null && typeof selectedAnswers[index] === 'number' && question.answers[selectedAnswers[index]] === question.correctAnswer) {
        totalScore++;
      }
    });
    setScore(totalScore);
    setSubmitted(true);
    try {
      const currentDate = new Date().toISOString().split('T')[0];
      const leaderboardEntry = { score: totalScore, date: currentDate };
      const existingData = await AsyncStorage.getItem('leaderboardData');
      const parsedData = existingData ? JSON.parse(existingData) : [];
      const newData = [...parsedData, leaderboardEntry];
      await AsyncStorage.setItem('leaderboardData', JSON.stringify(newData));
    } catch (error) {
      console.error('Error saving score to AsyncStorage:', error);
    }


    setCurrentPage('Leaderboard');
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Text style={styles.title}>Exam</Text>
        {shuffledQuestions.map((question, index) => (
          <View key={index} style={styles.questionContainer}>
            <Text style={styles.question}>{question.question}</Text>
            {question.answers.map((answer, answerIndex) => (
              <TouchableOpacity
                key={answerIndex}
                style={[
                  styles.answer,
                  selectedAnswers[index] === answerIndex && styles.selectedAnswer,
                ]}
                onPress={() => handleAnswer(index, answerIndex)}
                disabled={submitted}
              >
                <Text>{answer}</Text>
              </TouchableOpacity>
            ))}
          </View>
        ))}
        <Button title="Submit" onPress={handleSubmit} />
      </View>
    </ScrollView>
  );
};


const App: React.FC = () => {
  const [currentPage, setCurrentPage] = useState<'Leaderboard' | 'Exam'>('Leaderboard');

  const resetData = async () => {
    try {
      await AsyncStorage.clear();
      setCurrentPage('Exam')
      setCurrentPage('Leaderboard')
      console.log('Data cleared successfully');
    } catch (error) {
      console.error('Error clearing data:', error);
    }
  };

  return (
    <View style={styles.container}>
      {currentPage === 'Leaderboard' && <LeaderboardPage />}
      {currentPage === 'Exam' && <ExamPage setCurrentPage={setCurrentPage} />}
      <Button
        title="Go to Leaderboard"
        onPress={() => setCurrentPage('Leaderboard')}
        disabled={currentPage === 'Leaderboard'}
      />
      <Button
        title="Go to Exam"
        onPress={() => setCurrentPage('Exam')}
        disabled={currentPage === 'Exam'}
      />
      <Button
        title="Clear Data"
        onPress={() => resetData()}
        disabled={currentPage === 'Exam'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  questionContainer: {
    marginBottom: 20,
    width: 200,
  },
  question: {
    fontSize: 20,
    marginBottom: 10,
  },
  answer: {
    padding: 10,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    marginBottom: 10,
  },
  selectedAnswer: {
    backgroundColor: 'lightblue',
  },
});


export default App;
