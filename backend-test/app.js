const express = require('express');
const app = express();
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const database = 'database.json';

app.use(express.json());

// functions
function readTodos() {
    try {
        const data = fs.readFileSync(database);
        return JSON.parse(data);
    } catch (error) {
        return [];
    }
}

function writeTodos(todos) {
    fs.writeFileSync(database, JSON.stringify(todos, null, 2));
}

// route
app.get('/todos', (req, res) => {
    const todos = readTodos();
    res.json(todos);
});
app.post('/todos', (req, res) => {
    const { task } = req.body;
    if (!task) {
        return res.status(400).json({ error: 'Task is required' });
    }
    const newTodo = { id: uuidv4(), task };
    const todos = readTodos();
    todos.push(newTodo);
    writeTodos(todos);
    res.json(newTodo);
});
app.put('/todos/:id', (req, res) => {
    const { id } = req.params;
    const { task } = req.body;

    if (!task) {
        return res.status(400).json({ error: 'Task is required' });
    }

    const todos = readTodos();
    const index = todos.findIndex(todo => todo.id === id);

    // Check if todo with the given ID exists
    if (index === -1) {
        return res.status(404).json({ error: 'Todo not found' });
    }

    // Update the todo with the new data
    todos[index].task = task;
    writeTodos(todos);

    res.json({ message: 'Todo updated successfully', updatedTodo: todos[index] });
});
app.delete('/todos/:id', (req, res) => {
    const { id } = req.params;

    const todos = readTodos();
    const index = todos.findIndex(todo => todo.id === id);

    // Check if todo with the given ID exists
    if (index === -1) {
        return res.status(404).json({ error: 'Todo not found' });
    }

    // Remove the todo from the list
    todos.splice(index, 1);
    writeTodos(todos);

    res.json({ message: `Todo deleted successfully id : ${id}` });
});

// Get the application's router
const router = app._router.stack;

// Print all paths with their corresponding HTTP methods
console.log('Registered Routes:');
console.log('---------------------------------')
router.forEach(layer => {
    if (layer.route) {
        console.log(`${layer.route.stack[0].method.toUpperCase()} -> ${layer.route.path}`);
    }
});
console.log('---------------------------------')


const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});